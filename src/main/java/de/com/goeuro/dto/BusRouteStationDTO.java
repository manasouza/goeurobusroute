package de.com.goeuro.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BusRouteStationDTO {

	@JsonProperty("dep_sid")
	private Integer depSid;
	
	@JsonProperty("arr_sid")
	private Integer arrSid;
	
	@JsonProperty("direct_bus_route")
	private boolean directBusRoute;
}
