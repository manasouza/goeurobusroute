package de.com.goeuro;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import de.com.goeuro.DataFileResourceLoader.DataFileListener;

@SpringBootApplication
@ComponentScan
public class BusRouteApplication {

	private static final String DATA_FILE_PROTOCOL = "file";
	
	private static final Logger log = LoggerFactory.getLogger(BusRouteApplication.class); 

	public static void main(String[] args) {
		if (args.length <= 0) {
			log.error("No data file. Exiting application");
			System.exit(0);
		}
		try {
			String busRouteDataFilePath = args[0];
			ConfigurableApplicationContext appCtx = SpringApplication.run(BusRouteApplication.class, args);
			DataFileResourceLoader bean = appCtx.getBean(DataFileResourceLoader.class);
			bean.addDataListener(appCtx.getBean(DataFileListener.class));
			bean.loadBusRouteDataFile(DATA_FILE_PROTOCOL, busRouteDataFilePath);
		} catch (IOException e) {
			log.error("Error during data file analysis");
		}
	}
}
