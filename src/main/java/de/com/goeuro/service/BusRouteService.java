package de.com.goeuro.service;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.com.goeuro.DataFileResourceLoader;
import de.com.goeuro.DataFileResourceLoader.DataFileListener;
import de.com.goeuro.entity.BusRoute;
import de.com.goeuro.entity.Station;

@Service
public class BusRouteService implements DataFileListener {
	
	@Autowired
	private DataFileResourceLoader resourceLoader;
	
	private Map<BusRoute, Collection<Station>> busRouteMap;

	public boolean directBusRoute(Integer stationDepartureId, Integer stationArrivalId) {
		boolean directBusRoute = false;
		Station departureStation = new Station(Integer.valueOf(stationDepartureId));
		Station arrivalStation = new Station(Integer.valueOf(stationArrivalId));
		for (BusRoute busRoute : busRouteMap.keySet()) {
			if (busRouteMap.get(busRoute).contains(departureStation) && busRouteMap.get(busRoute).contains(arrivalStation)) {
				directBusRoute = true;
				break;
			}
		}
		return directBusRoute;
	}

	@Override
	public void dataLoaded() {
		this.busRouteMap = this.resourceLoader.getBusRouteMap();
	}

}
