package de.com.goeuro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.com.goeuro.dto.BusRouteStationDTO;
import de.com.goeuro.service.BusRouteService;

@RestController
@RequestMapping("/api")
public class BusRouteController {
	
	@Autowired
	private BusRouteService service;

	@RequestMapping(path="/direct", method=RequestMethod.GET)
	public @ResponseBody BusRouteStationDTO hasValidRoute(@RequestParam("dep_sid") Integer departureStationId,
													   @RequestParam("arr_sid") Integer arrivalStationId) {
		boolean directBusRoute = service.directBusRoute(departureStationId, arrivalStationId);
		return new BusRouteStationDTO(departureStationId, arrivalStationId, directBusRoute);
	}
}
