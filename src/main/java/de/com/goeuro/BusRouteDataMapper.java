package de.com.goeuro;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;

import de.com.goeuro.entity.BusRoute;
import de.com.goeuro.entity.Station;
import de.com.goeuro.validation.BusRouteValidator;

@Component
public class BusRouteDataMapper {

	static final int BUS_ROUTE_LIMIT = 100000;
	static final int STATION_ROUTE_LIMIT = 1000;
	
	private static final String ROUTE_ID_SEPARATOR = " ";
	private static final int BUS_ROUTE_COUNT_INDEX = 0;

	public Map<BusRoute, Collection<Station>> mapToBusRouteStations(List<String> busRouteDataList) throws BusRouteValidator {
		Map<BusRoute, Collection<Station>> map = Maps.newHashMap();
		try {
			Integer busRouteCount = Integer.parseInt(busRouteDataList.remove(BUS_ROUTE_COUNT_INDEX));
			validateBusRouteCount(busRouteCount, busRouteDataList.size());
			for (String busRouteData : busRouteDataList) {
				BusRoute busRoute = null;
				String[] busRouteElements = busRouteData.split(ROUTE_ID_SEPARATOR);
				validateStationCount(busRouteElements.length - 1);
				for (String busRouteElement : busRouteElements) {
					if (busRoute == null) {
						busRoute = new BusRoute(Integer.parseInt(busRouteElement));
						map.put(busRoute, emptyStations());					
					} else {
						Collection<Station> stations = map.get(busRoute);
						stations.add(new Station(Integer.parseInt(busRouteElement)));
					}
				}
			}		
		} catch (NumberFormatException e) {
			throw new BusRouteValidator(e.getMessage());
		}
		return map;
	}

	private Collection<Station> emptyStations() {
		return new ArrayList<Station>();
	}

	private void validateStationCount(Integer stationSize) throws BusRouteValidator {
		if (stationSize > STATION_ROUTE_LIMIT) {
			throw new BusRouteValidator("Station route count overflows limit of: " + STATION_ROUTE_LIMIT);
		}
	}
	
	private void validateBusRouteCount(Integer busRouteCount, Integer busRouteContentLength) throws BusRouteValidator {
		if (busRouteCount > BUS_ROUTE_LIMIT) {
			throw new BusRouteValidator("Bus Route count overflows limit of: " + BUS_ROUTE_LIMIT);
		} else if (!busRouteCount.equals(busRouteContentLength)) {
			throw new BusRouteValidator("Divergence between Bus Route count (" + busRouteCount + ") and content ("+ busRouteContentLength +")");
		}
	}
}
