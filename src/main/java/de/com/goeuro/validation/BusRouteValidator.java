package de.com.goeuro.validation;

public class BusRouteValidator extends Exception {

	private static final long serialVersionUID = 6094601910388253725L;

	public BusRouteValidator(String message) {
		super(message);
	}

}
