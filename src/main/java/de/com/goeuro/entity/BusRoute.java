package de.com.goeuro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BusRoute {

	private Integer id;

}
