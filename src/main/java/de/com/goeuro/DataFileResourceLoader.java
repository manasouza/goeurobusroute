package de.com.goeuro;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import de.com.goeuro.entity.BusRoute;
import de.com.goeuro.entity.Station;
import de.com.goeuro.validation.BusRouteValidator;

@Component
public class DataFileResourceLoader implements ResourceLoaderAware {
	
	private static final Logger log = LoggerFactory.getLogger(BusRouteApplication.class);

	public interface DataFileListener {

		public void dataLoaded();

	}

	private ResourceLoader resourceLoader;
	private Map<BusRoute, Collection<Station>> busRouteMap;
	private DataFileListener listener;
	
	@Autowired
	private BusRouteDataMapper mapper;

	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
	
	public void loadBusRouteDataFile(String protocol, String busRouteDataFilePath) throws IOException {
		Resource resource = resourceLoader.getResource(protocol+":"+busRouteDataFilePath);
		Path path = resource.getFile().toPath();
		setBusRouteDataContent(Files.readAllLines(path));
	}

		
	private void setBusRouteDataContent(List<String> content) {
		try {
			setBusRouteMap(mapper.mapToBusRouteStations(content));
			listener.dataLoaded();
		} catch (BusRouteValidator e) {
			log.error(e.getMessage(), e);
		}
	}
	
	public Map<BusRoute, Collection<Station>> getBusRouteMap() {
		return busRouteMap;
	}
	
	public void setBusRouteMap(Map<BusRoute, Collection<Station>> busRouteMap) {
		this.busRouteMap = busRouteMap;
	}

	public void addDataListener(DataFileListener dataListener) {
		this.listener = dataListener;
	}

}
