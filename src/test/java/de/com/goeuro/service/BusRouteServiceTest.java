package de.com.goeuro.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.com.goeuro.DataFileResourceLoader;
import de.com.goeuro.validation.BusRouteValidator;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BusRouteServiceTest {
	
	@Autowired
	private BusRouteService service;
	
	@Autowired
	private DataFileResourceLoader dataFileResourceLoader;
	
	@Before
	public void setUp() throws IOException {
		dataFileResourceLoader.addDataListener(service);
		// load file from src/test/resources
		dataFileResourceLoader.loadBusRouteDataFile("classpath", "/bus_route_data");
	}

	@Test
	public void noRouteFoundForStations() {
		// GIVEN
		Integer stationDepartureId = 3;
		Integer stationArrivalId = 9;
		
		// WHEN
		boolean directBusRoute = service.directBusRoute(stationDepartureId, stationArrivalId);
		
		// THEN
		assertFalse(directBusRoute);
	}
	
	@Test
	public void routeFoundForStations() throws BusRouteValidator {
		// GIVEN
		Integer stationDepartureId = 3;
		Integer stationArrivalId = 6;

		// WHEN
		boolean directBusRoute = service.directBusRoute(stationDepartureId, stationArrivalId);

		// THEN
		assertTrue(directBusRoute);
	}
}
