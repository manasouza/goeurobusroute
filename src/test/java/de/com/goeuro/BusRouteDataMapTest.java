package de.com.goeuro;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

import de.com.goeuro.entity.BusRoute;
import de.com.goeuro.entity.Station;
import de.com.goeuro.validation.BusRouteValidator;

public class BusRouteDataMapTest {
	
	private BusRouteDataMapper busRouteDataMapper;

	@Before
	public void setUp() {
		busRouteDataMapper = new BusRouteDataMapper();		
	}
	
	@Test(expected=BusRouteValidator.class)
	public void busRouteCountExceedsLimit() throws BusRouteValidator {
		// GIVEN
		String routeSize = String.valueOf(BusRouteDataMapper.BUS_ROUTE_LIMIT + 10);
		String[] routeIds = new String[] {"0 ", "1 ", "2 "};
		String[] stationIds = new String[] {"0 1 2 3 4", "3 1 6 5", "0 6 4"};
		List<String> busRouteData = createFileStringList(routeSize , routeIds, stationIds);
		
		// WHEN
		busRouteDataMapper.mapToBusRouteStations(busRouteData);
	}
	
	@Test(expected=BusRouteValidator.class)
	public void busRouteCountDoesNotMatchContentLength() throws BusRouteValidator {
		// GIVEN
		String routeSize = "10";
		String[] routeIds = new String[] {"0 ", "1 ", "2 "};
		String[] stationIds = new String[] {"0 1 2 3 4", "3 1 6 5", "0 6 4"};
		List<String> busRouteData = createFileStringList(routeSize , routeIds, stationIds);

		// WHEN
		busRouteDataMapper.mapToBusRouteStations(busRouteData);
	}
	
	@Test(expected=BusRouteValidator.class)
	public void invalidBusRouteCount() throws BusRouteValidator {
		// GIVEN
		String routeSize = "abc";
		String[] routeIds = new String[] {"0 ", "1 ", "2 "};
		String[] stationIds = new String[] {"0 1 2 3 4", "3 1 6 5", "0 6 4"};
		List<String> busRouteData = createFileStringList(routeSize , routeIds, stationIds);

		// WHEN
		busRouteDataMapper.mapToBusRouteStations(busRouteData);
	}
	
	@Test
	public void mapDataFileStringToEntities() throws BusRouteValidator {
		// GIVEN
		String routeSize = "3";
		String[] routeIds = new String[] {"0 ", "1 ", "2 "};
		String[] stationIds = new String[] {"0 1 2 3 4", "3 1 6 5", "0 6 4"};
		List<String> busRouteData = createFileStringList(routeSize , routeIds, stationIds);
		
		Map<BusRoute, Collection<Station>> map = busRouteDataMapper.mapToBusRouteStations(busRouteData);
		
		// THEN
		assertEquals(Integer.parseInt(routeSize), map.size());
		assertTrue(map.containsKey(new BusRoute(0)));
		assertTrue(map.containsKey(new BusRoute(1)));
		assertTrue(map.containsKey(new BusRoute(2)));
		assertEquals(5, map.get(new BusRoute(0)).size());
		assertEquals(4, map.get(new BusRoute(1)).size());
		assertEquals(3, map.get(new BusRoute(2)).size());
	}

	private List<String> createFileStringList(String routeSize, String[] routeIds, String[] stationIds) {
		List<String> dataList = Lists.newArrayList(routeSize);
		for (String routeId : routeIds) {
			dataList.add(routeId + stationIds[Integer.valueOf(routeId.trim())]);
		}
		return dataList;
	}
}
